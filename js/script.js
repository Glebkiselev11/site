
// нав бар

const navBtn = document.querySelector('.navbar__button');
const navMenu = document.querySelector('.menu');
const navMenuLink = navMenu.querySelectorAll('.menu__link');

navBtn.onclick = () => {
  navMenu.classList.toggle('menu_active');
  navBtn.classList.toggle('navbar__button_active');
};

for (let i = 0; i < navMenuLink.length; i++) {
  navMenuLink[i].onclick = () => {
    navMenu.classList.remove('menu_active');
    navBtn.classList.remove('navbar__button_active');
  }
};






//плавная прокрутка нав бара

const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()
    
    const blockID = anchor.getAttribute('href')
    
    document.querySelector('' + blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}

//кнопка вверх и вниз для скролинга страницы
var updownElem = document.getElementById('updown');

    var pageYLabel = 0;

    updownElem.onclick = function() {
      var pageY = window.pageYOffset || document.documentElement.scrollTop;

      switch (this.className) {
        case 'up':
          pageYLabel = pageY;
          window.scrollTo(0, 0);
          this.className = 'down';
          break;

        case 'down':
          window.scrollTo(0, pageYLabel);
          this.className = 'up';
      }

    }

    window.onscroll = function() {
      var pageY = window.pageYOffset || document.documentElement.scrollTop;
      var innerHeight = document.documentElement.clientHeight;

      switch (updownElem.className) {
        case '':
          if (pageY > innerHeight) {
            updownElem.className = 'up';
          }
          break;

        case 'up':
          if (pageY < innerHeight) {
            updownElem.className = '';
          }
          break;

        case 'down':
          if (pageY > innerHeight) {
            updownElem.className = 'up';
          }
          break;

      }
    }

// preloader

document.body.onload = function(){

    setTimeout(function() {
        var preloader = document.getElementById('page-preloader');
        if( !preloader.classList.contains('done'))
        {
            preloader.classList.add('done');
        }
    }, 500);
}