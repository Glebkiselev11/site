
var coffeeOur = document.querySelector('.coffee-our');
var coffeeContender = document.querySelector('.coffee-contender');

var coffeeOurInput = document.querySelector('#coffee-Our');
coffeeOurInput.addEventListener ('change', function() {
  coffeeOur.classList.add('selected');
  coffeeContender.classList.remove('selected');
});

var coffeeContenderInput = document.querySelector('#coffee-contender');
coffeeContenderInput.addEventListener('change', function(){
  coffeeContender.classList.add('selected');
  coffeeOur.classList.remove('selected');
});

var teaOur = document.querySelector('.tea-our');
var teaContender = document.querySelector('.tea-contender');

var teaOurInput = document.querySelector('#tea-Our');
teaOurInput.addEventListener ('change', function() {
  teaOur.classList.add('selected');
  teaContender.classList.remove('selected');
});

var teaContenderInput = document.querySelector('#tea-contender');
teaContenderInput.addEventListener('change', function(){
  teaContender.classList.add('selected');
  teaOur.classList.remove('selected');
});

var syrupOur = document.querySelector('.syrup-our');
var syrupContender = document.querySelector('.syrup-contender');

var syrupOurInput = document.querySelector('#syrup-Our');
syrupOurInput.addEventListener ('change', function() {
  syrupOur.classList.add('selected');
  syrupContender.classList.remove('selected');
});

var syrupContenderInput = document.querySelector('#syrup-contender');
syrupContenderInput.addEventListener('change', function(){
  syrupContender.classList.add('selected');
  syrupOur.classList.remove('selected');
});

var ConcentratesOur = document.querySelector('.Concentrates-our');
var ConcentratesContender = document.querySelector('.Concentrates-contender');

var ConcentratesOurInput = document.querySelector('#Concentrates-Our');
ConcentratesOurInput.addEventListener ('change', function() {
  ConcentratesOur.classList.add('selected');
  ConcentratesContender.classList.remove('selected');
});

var ConcentratesContenderInput = document.querySelector('#Concentrates-contender');
ConcentratesContenderInput.addEventListener('change', function(){
  ConcentratesContender.classList.add('selected');
  ConcentratesOur.classList.remove('selected');
});

var toppingOur = document.querySelector('.topping-our');
var toppingContender = document.querySelector('.topping-contender');

var toppingOurInput = document.querySelector('#topping-Our');
toppingOurInput.addEventListener ('change', function() {
  toppingOur.classList.add('selected');
  toppingContender.classList.remove('selected');
});

var toppingContenderInput = document.querySelector('#topping-contender');
toppingContenderInput.addEventListener('change', function(){
  toppingContender.classList.add('selected');
  toppingOur.classList.remove('selected');
});

var sugarOur = document.querySelector('.sugar-our');
var sugarContender = document.querySelector('.sugar-contender');

var sugarOurInput = document.querySelector('#sugar-Our');
sugarOurInput.addEventListener ('change', function() {
  sugarOur.classList.add('selected');
  sugarContender.classList.remove('selected');
});

var sugarContenderInput = document.querySelector('#sugar-contender');
sugarContenderInput.addEventListener('change', function(){
  sugarContender.classList.add('selected');
  sugarOur.classList.remove('selected');
});

var chocolateOur = document.querySelector('.chocolate-our');
var chocolateContender = document.querySelector('.chocolate-contender');

var chocolateOurInput = document.querySelector('#chocolate-Our');
chocolateOurInput.addEventListener ('change', function() {
  chocolateOur.classList.add('selected');
  chocolateContender.classList.remove('selected');
});

var chocolateContenderInput = document.querySelector('#chocolate-contender');
chocolateContenderInput.addEventListener('change', function(){
  chocolateContender.classList.add('selected');
  chocolateOur.classList.remove('selected');
});

var creamOur = document.querySelector('.cream-our');
var creamContender = document.querySelector('.cream-contender');

var creamOurInput = document.querySelector('#cream-Our');
creamOurInput.addEventListener ('change', function() {
  creamOur.classList.add('selected');
  creamContender.classList.remove('selected');
});

var creamContenderInput = document.querySelector('#cream-contender');
creamContenderInput.addEventListener('change', function(){
  creamContender.classList.add('selected');
  creamOur.classList.remove('selected');
});

var cappuccinoOur = document.querySelector('.cappuccino-our');
var cappuccinoContender = document.querySelector('.cappuccino-contender');

var cappuccinoOurInput = document.querySelector('#cappuccino-Our');
cappuccinoOurInput.addEventListener ('change', function() {
  cappuccinoOur.classList.add('selected');
  cappuccinoContender.classList.remove('selected');
});

var cappuccinoContenderInput = document.querySelector('#cappuccino-contender');
cappuccinoContenderInput.addEventListener('change', function(){
  cappuccinoContender.classList.add('selected');
  cappuccinoOur.classList.remove('selected');
});