document.querySelector('.open-button').onclick = function() {
  this.classList.add('open-button_active');
  document.querySelector('.call-back').classList.add('call-back_show');
};

document.querySelector('.close-button').onclick = function() {
  document.querySelector('.open-button').classList.remove('open-button_active');
  document.querySelector('.call-back').classList.remove('call-back_show');
};
