
//нав бар

const navbarButton = document.querySelector('.navbar__button');
const navbar = document.querySelector('.navbar__list');
const main = document.querySelectorAll('.shift');
const navbarLink = document.querySelectorAll('.navbar__link')

navbarButton.onclick = function () {
  navbarButton.classList.toggle('navbar__button_active');
  navbar.classList.toggle('navbar__list_active');
  
  for (let i = 0; i < main.length; i++) {
    main[i].classList.toggle('shift-left');
  }
}


for (let i = 0; i < navbarLink.length; i++) {
  navbarLink[i].onclick = function () {
    navbar.classList.remove('navbar__list_active');
    navbarButton.classList.remove('navbar__button_active');
    for (let x = 0; x < main.length; x++) {
      main[x].classList.remove('shift-left');
    }
  }
}





//плавная прокрутка нав бара

const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()
    
    const blockID = anchor.getAttribute('href')
    
    document.querySelector('' + blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}
